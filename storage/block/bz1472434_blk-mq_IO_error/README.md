# storage/block/bz1472434_blk-mq_IO_error
Storage: bz1472434_blk-mq_IO_error

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
